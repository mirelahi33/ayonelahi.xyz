<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    public function department()
    {
        return $this->belongsTo('App\Department');
    }
    public function room()
    {
        return $this->belongsTo('App\Room');
    }
    public function subject()
    {
        return $this->belongsTo('App\Subject');
    }
    public function section()
    {
        return $this->belongsTo('App\Section');
    }
    public function semester()
    {
        return $this->belongsTo('App\Semester');
    }
    public function year()
    {
        return $this->belongsTo('App\Year');
    }
    public function results()
    {
        return $this->hasMany('App\Result');
    }
    

    // use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','code','date','department_id','room_id','subject_id','section_id','semester_id','year_id'
    ];
}
