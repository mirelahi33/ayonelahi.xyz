<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    public function student()
    {
        return $this->belongsTo('App\Student');
    }
    public function exam()
    {
        return $this->belongsTo('App\Exam');
    }
    public function subject()
    {
        return $this->belongsTo('App\Subject');
    }
    // use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'score','exam_id','student_id'
    ];
}
