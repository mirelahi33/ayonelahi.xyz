<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    public function exams()
    {
        return $this->hasMany('App\Exam');
    }
    protected $fillable = [
        'code','floor',
    ];
}
