<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Year;
use App\Http\Resources\YearCollection;
use App\Http\Resources\YearResource;

class YearController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   $allYear=Year::orderBy('id','DESC')->paginate(10);
        // return Department::latest()->paginate(10);

        return new YearCollection($allYear);
    }

    public function search($field,$query)
    {
        return new YearCollection(Year::where($field,'LIKE',"%$query%")->latest()->paginate(10));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|string|max:191',
            'code'=>'required|string|max:191|unique:years',
            'starting_year'=>'required|string|max:191',
            'ending_year'=>'required|string|max:191',
        ]);

        return Year::create([
            'name'=>$request['name'],
            'code'=>$request['code'],
            'starting_year'=>$request['starting_year'],
            'ending_year'=>$request['ending_year'],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $year = Year::findOrFail($id);

        $this->validate($request,[
            'name'=>'required|string|max:191',
            'code'=>'required|string|max:191|unique:years,code,'.$id,
            'starting_year'=>'required|string|max:191',
            'ending_year'=>'required|string|max:191',
        ]);

        $year->update($request->all());
        return ['message' => 'Updated the Acadmemic Year info'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $year = Year::findOrFail($id);
        // delete the user

        $year->delete();

        return ['message' => 'Academic Year Deleted'];
    }
}
