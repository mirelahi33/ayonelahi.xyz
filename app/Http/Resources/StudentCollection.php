<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class StudentCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->transform(function ($student){
                return [
                    'id' => $student->id,
                    'name' => $student->name,
                    'code' => $student->code,
                    'department'=>$student['department'],
                    'year'=>$student['year'],
                    'semester'=>$student['semester'],
                    'section'=>$student['section'],
                    'batch'=>$student['batch'],
                //     'department_id' => $student->department_id,
                //     'year_id' => $student->year_id,
                //     'semester_id' => $student->semester_id,
                //     'section_id' => $student->section_id,
                //     'batch_id' => $student->batch_id,
                // 
            ];
            })
        ];
    }
}
