<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class YearCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->transform(function ($year){
                return [
                    // $year
                    'id' => $year->id,
                    'name' => $year->name,
                    'code' => $year->code,
                    'starting_year' => $year->starting_year,
                    'ending_year' => $year->ending_year,
                ];
            })
        ];
    }
}
