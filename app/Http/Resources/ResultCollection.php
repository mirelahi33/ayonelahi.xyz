<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ResultCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->transform(function ($result){
                return [
                    // $result
                    'id' => $result->id,
                    'score' => $result->score,
                    'exam'=>$result['exam'],
                    'student'=>$result['student'],
                 
                ];
            })
        ];
    }
}
