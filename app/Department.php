<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = [
        'name','code',
    ];

    public function subjects()
    {
        return $this->hasMany('App\Subject');
    }
    public function exams()
    {
        return $this->hasMany('App\Exam');
    }
}
