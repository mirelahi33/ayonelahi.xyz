<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Batch extends Model
{
    protected $fillable = [
        'name','code',
    ];

    public function students()
    {
        return $this->hasMany('App\Student');
    }
}
