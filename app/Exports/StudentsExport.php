<?php

namespace App\Exports;

use App\Student;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class StudentsExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function headings(): array
    {
        return [
            'Name',
            'Department',
            'Section',
        ];
    }
    public function collection()
    {
        $allStudent=Student::with('department','section')->get();
        $arr =array();
        $arr2 =array();
        foreach($allStudent as $e) {
          
                $data =  array($e->name,$e->department->name,$e->section->name);
           
                array_push($arr, $data);
               
            }
            $aa=collect($arr);
            // dd($aa);
        return $aa;
    }
}
