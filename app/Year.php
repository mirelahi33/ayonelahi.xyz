<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Year extends Model
{
    protected $fillable = [
        'name','code','starting_year','ending_year',
    ];

    public function exams()
    {
        return $this->hasMany('App\Exam');
    }
    public function students()
    {
        return $this->hasMany('App\Student');
    }
}
