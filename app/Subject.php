<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Notifications\Notifiable;

class Subject extends Model
{

    public function exams()
    {
        return $this->hasMany('App\Exam');
    }
    public function department()
    {
        return $this->belongsTo('App\Department');
    }
    public function results()
    {
        return $this->hasMany()('App\Result');
    }
    // protected $table = 'subjects';
    //
    // use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','code','department_id'
    ];
}
