<?php

use Faker\Generator as Faker;

$factory->define(App\Section::class, function (Faker $faker) {
    return [
        'name'=>$faker->unique()->randomElement([
            'A',
            'B',
            'C'
            ]),
        'code'=>$faker->randomElement([
            '01',
            '02',
            '03'
            ]),
    ];
});
